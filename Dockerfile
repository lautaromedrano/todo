FROM node:latest

WORKDIR /var/www/

COPY package*.json ./

RUN npm install -g @adonisjs/cli

RUN npm install

COPY . .

EXPOSE 3333

CMD ["adonis", "serve", "--dev"]
