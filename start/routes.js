'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/login', 'UserController.index').as('login').middleware(['authCheck']);
Route.post('/login', 'UserController.login');
Route.get('/logout', 'UserController.logout');

Route.get('/register', 'UserController.register').middleware(['authCheck']);
Route.post('/register', 'UserController.create');

Route.get('/items', 'ItemController.index').as('home').middleware(['auth']);
Route.post('/items', 'ItemController.store').middleware(['auth']);
Route.delete('/items/:id', 'ItemController.destroy').middleware(['auth']);

Route.get('/categories', 'CategoryController.index').middleware(['auth']);
Route.post('/categories', 'CategoryController.store').middleware(['auth']);
Route.delete('/categories/:id', 'CategoryController.destroy').middleware(['auth']);
