'use strict';

class AuthCheck {
    async handle({request, response, auth}, next) {
        try {
            await auth.check();

            response.route('home');
        } catch (e) {
            await next();
        }
    }
}

module.exports = AuthCheck;