'use strict';

const User = use('App/Models/User');

class UserController {

    async index({view}) {
        return view.render('users.login');
    }

    async register({view}) {
        return view.render('users.register');
    }

    async create({request, response, auth, session}) {
        let {username, password} = request.all();
        let user = await User.create({username, password});

        return response.redirect('/login');
    }

    async login({request, response, auth, session}) {
        let {username, password} = request.all()

        try {
            if (await auth.attempt(username, password)) {
                return response.route('home');
            }
        } catch (e) {
            console.log(e)
            session.flash({ notification: "Usuario/contraseña incorrecta." });

            return response.redirect('/login');
        }
    }

    async logout({auth, response}) {
        await auth.logout();

        response.route('login');
    }
}

module.exports = UserController;
