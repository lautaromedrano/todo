'use strict'

const config = use('Config');
const Item = use('App/Models/Item.js');
const Category = use('App/Models/Category.js');
const { validate } = use('Validator');

class ItemController {
    async index({auth, view}) {
        const user = auth.current.user;
        const items = await user.items().with('category').fetch();
        const categories = await Category.all();

        return view.render('items.index', {items: items.toJSON(), categories: categories.toJSON()});
    }

    async store({ request, response, session, auth }) {
      const validation = await validate(request.all(), {content: 'required|min:3|max:255', category_id: 'required'});

      if (validation.fails()) {
        session.withErrors(validation.messages()).flashAll();

        return response.redirect('back');
      }

      const user = auth.current.user;

      const item = new Item();
      item.content = request.input('content');
      item.category_id = request.input('category_id');
      await user.items().save(item);

      session.flash({ notification: {text: 'Item agregado!', sentiment: 'success'} });

      return response.redirect('back');
    }

    async destroy({ auth, params, session, response }) {
        const user = auth.current.user;
        const item = await Item.find(params.id);

        await user.items().detach([params.id]);
        await item.delete();

        session.flash({ notification: {text: 'Item eliminado!', sentiment: 'success'} });

        return response.redirect('back');
    }
}

module.exports = ItemController
