'use strict'

const config = use('Config');
const Category = use('App/Models/Category.js');
const Item = use('App/Models/Item.js');
const { validate } = use('Validator');

class CategoryController {
    async index({view}) {
        const categories = await Category.all();

        return view.render('categories.index', {categories: categories.toJSON()});
    }

    async store({ request, response, session }) {
      const validation = await validate(request.all(), {name: 'required|min:3|max:255'});

      if (validation.fails()) {
        session.withErrors(validation.messages()).flashAll();

        return response.redirect('back');
      }

      const category = new Category();
      category.name = request.input('name');
      await category.save();

      session.flash({ notification: {text: 'Categoria agregada!', sentiment: 'success'} });

      return response.redirect('back');
    }

    async destroy({ params, session, response }) {
      const category = await Category.find(params.id);
      if (!category) {
        session.flash({ notification: {text: 'La categoría no existe!', sentiment: 'error'} });

        return response.redirect('back');
      }

      const itemsCountForCategory = await Item.query().where('category_id', params.id).getCount();
      if (itemsCountForCategory > 0) {
        session.flash({ notification: {text: 'La categoría contiene items, no se puede eliminar!', sentiment: 'error'} });

        return response.redirect('back');
      } 

      await category.delete();

      session.flash({ notification: {text: 'Categoria eliminada!', sentiment: 'success'} });

      return response.redirect('back');
    }
}

module.exports = CategoryController;
