'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ItemUserSchema extends Schema {
  up () {
    this.create('item_user', (table) => {
      table.increments();
      table.integer('user_id').unsigned().notNullable().references('users.id');
      table.integer('item_id').unsigned().notNullable().references('items.id');
      table.timestamps();
    })
  }

  down () {
    this.drop('item_user');
  }
}

module.exports = ItemUserSchema;
