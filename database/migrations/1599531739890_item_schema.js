'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TasksSchema extends Schema {
  up () {
    this.create('items', (table) => {
      table.increments();
      table.string('content').notNullable();
      table.timestamps();
    })
  }

  down () {
    this.drop('items')
  }
}

module.exports = TasksSchema
