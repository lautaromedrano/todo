'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DataTagSchema extends Schema {
  up () {
    this.create('item_tag', (table) => {
      table.increments();
      table.integer('item_id').unsigned().notNullable().references('items.id');
      table.integer('tag_id').unsigned().notNullable().references('tags.id');
      table.timestamps();
    })
  }

  down () {
    this.drop('item_tag')
  }
}

module.exports = DataTagSchema
