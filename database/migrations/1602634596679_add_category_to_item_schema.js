'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategoryItemSchema extends Schema {
  up () {
    this.table('items', (table) => {
      table.integer('category_id').unsigned().notNullable().references('categories.id');
    })
  }

  down () {
    this.dropColumn('category_id');
  }
}

module.exports = CategoryItemSchema;
